﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] string sceneName;
    public void PlayGame()
    {
        SceneManager.LoadScene("Scene1");
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void NextLevel()
    {
        SceneManager.LoadScene("Scene3");
    }
    public void MainManu()
    {
        SceneManager.LoadScene("Menu");
    }
    private void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene(sceneName);
    }
    public void Restart()
    {
        SceneManager.LoadScene("Scene1");
    }
}
