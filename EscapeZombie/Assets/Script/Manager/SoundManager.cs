﻿using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    [SerializeField] AudioMixer audioMixer;
    private static AudioClip playerFireSound;
    private static AudioClip playerReloadSound;
    private static AudioSource audioSource;

    void Start()
    {
        playerFireSound = Resources.Load<AudioClip>("Shot");
        playerReloadSound = Resources.Load<AudioClip>("Reload");
        audioSource = GetComponent<AudioSource>();
    }
    public static void PlayerSound(string clip)
    {
        switch (clip)
        {
            case "Shot":
                audioSource.PlayOneShot(playerFireSound);
                break;
            case "Reload":
                audioSource.PlayOneShot(playerReloadSound);
                break;

        }
    }
    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }
}
