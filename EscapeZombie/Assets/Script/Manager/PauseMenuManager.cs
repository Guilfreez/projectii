﻿using UnityEngine;
using UnityEngine.Audio;


public class PauseMenuManager : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject pCamera;
    [SerializeField] private bool isPause;
    [SerializeField] GameObject Gun;
    [SerializeField] GameObject optionMenu;
    [SerializeField] AudioMixer audioMixer;

    void Start()
    {
        pauseMenu.SetActive(false);
        optionMenu.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPause = !isPause;
        }
        if (isPause)
        {
            PauseMenuOpen();
        }
        else
        {
            PauseMenuClose();
            PauseMenuClose();
        }
    }
    public void PauseMenuOpen()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        pCamera.GetComponent<MouseLook>().enabled = false;
        Gun.GetComponent<Shoot>().enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }
    public void PauseMenuClose()
    {
        pauseMenu.SetActive(false);
        optionMenu.SetActive(false);
        isPause = false;
        Time.timeScale = 1;
        pCamera.GetComponent<MouseLook>().enabled = true;
        Gun.GetComponent<Shoot>().enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }
    public void Option()
    {
        optionMenu.SetActive(true);
        pauseMenu.SetActive(false);
    }
    public void Back()
    {
        pauseMenu.SetActive(true);
        optionMenu.SetActive(false);
    }


}
