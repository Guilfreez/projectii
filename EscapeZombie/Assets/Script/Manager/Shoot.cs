﻿using System.Collections;
using UnityEngine;
using TMPro;

// Original From https://www.youtube.com/watch?v=THnivyG0Mvo
// Original From https://www.youtube.com/watch?v=kAx5g9V5bcM

public class Shoot : MonoBehaviour
{
    [SerializeField] float damage = 10f;
    [SerializeField] float range = 100f;
    [SerializeField] float fireRate = 10f;
    [SerializeField] Camera fpscam;
    [SerializeField] ParticleSystem muzzleFlash;
    [SerializeField] int maxAmmo = 12;
    [SerializeField] TextMeshProUGUI ammo;
    private int currentAmmo;
    private float reloadTime = 1.5f;
    private float nextTimeToFire = 0f;
    private bool isReloading = false;
    void Update()
    {
        if (isReloading)
        {
            return;
        }
        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }
        if (Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 2f / fireRate;
            Shot();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(Reload()); ;
        }
    }
    IEnumerator Reload()
    {
        isReloading = true;
        SoundManager.PlayerSound("Reload");
        yield return new WaitForSeconds(reloadTime);
        currentAmmo = maxAmmo;
        isReloading = false;
        UpdateAmmoText();
    }
    void Shot()
    {
        SoundManager.PlayerSound("Shot");
        currentAmmo--;
        muzzleFlash.Play();
        RaycastHit hit;
        if (Physics.Raycast(fpscam.transform.position, fpscam.transform.forward, out hit, range))
        {
            Enemy enemy = hit.transform.GetComponent<Enemy>();

            if (enemy != null)
            {
                enemy.Takehit(damage);
            }
        }
        UpdateAmmoText();
    }
    void UpdateAmmoText()
    {
        ammo.text = $" {currentAmmo}  /   {maxAmmo}";
    }
}