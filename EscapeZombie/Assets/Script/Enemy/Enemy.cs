﻿using UnityEngine;

// Original From https://www.youtube.com/watch?v=THnivyG0Mvo

public class Enemy : MonoBehaviour
{
    [SerializeField] float health = 50f;

    public void Takehit(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
    }
    void Die()
    {
        Destroy(gameObject);
    }
}
