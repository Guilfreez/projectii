﻿using UnityEngine;
using UnityEngine.AI;

//Original From https://www.youtube.com/watch?v=QcKR_YrCaMQ 


public class EnemyAi : MonoBehaviour
{
    [SerializeField] float enemyDamage = 10f;
    [SerializeField] float lookRadius = 10f;
    [SerializeField] float stopDistance = 2f;
    private float lastAttackTime = 0;
    private float attackCoolDown = 2;
    private Transform target;
    private NavMeshAgent agent;

    void Start()
    {
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
    }
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance <= lookRadius)
        {
            GoToTarget();
        }
        if (distance < stopDistance)
        {
            StopEnemy();
            Attack();
        }

    }
    private void GoToTarget()
    {
        agent.isStopped = false;
        agent.SetDestination(target.position);
    }
    private void StopEnemy()
    {
        agent.isStopped = true;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
    void Attack()
    {
        if (Time.time - lastAttackTime >= attackCoolDown)
        {
            lastAttackTime = Time.time;
            target.GetComponent<PlayerStatus>().Takehit(enemyDamage);
            target.GetComponent<PlayerStatus>().updateHp();

        }
    }
}
