﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

// Original From https://www.youtube.com/watch?v=THnivyG0Mvo

public class PlayerStatus : MonoBehaviour
{
    [SerializeField] float health = 50f;
    [SerializeField] TextMeshProUGUI hp;
    private float maxHp = 0;
    private void Start()
    {
        maxHp = health;
        updateHp();
    }
    public void Takehit(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
        updateHp();
    }
    public void Die()
    {
        Destroy(gameObject);
        SceneManager.LoadScene("Die");
    }
    public void updateHp()
    {
        hp.text = health + " / " + maxHp;
    }
}
